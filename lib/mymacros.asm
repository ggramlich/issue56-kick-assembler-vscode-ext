#importonce

/* exports macro fillScreen */
#import "common\lib\mem-global.asm"

.macro fillDefaultScreen(character) {
    /* In this file
       auto-completing the "fillScreen" macro does not work at all.
       VS Code offers a list of often used words, which might also
       contain fillScreen, but does not recognize it as a macro.
       
       When compiling from the main.asm with this file imported and
       the fillScreen line uncommented, it works fine.
    */

    /* macro from mem-global */
    //fillScreen($0400, 'x')
}

.macro changeBorderColor() {
    inc $d020
}
