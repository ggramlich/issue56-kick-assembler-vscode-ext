/*
Demo Project for
https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/56
*/

//#import "lib_mymacros.asm"
#import "lib/mymacros.asm"

/* exports macro fillScreen */
#import "common\lib\mem-global.asm"


BasicUpstart2(start)

start:
    fillDefaultScreen('x')
    
    /* In this file
       auto-completing the "fillScreen" macro from mem-global works sometimes.
       It works most reliably, when saving the file in a compilable state and then
       starting to type CTRL+SPACE on a blank line followed by fillSc.
       It sometimes works, when you start typing fillSc on a blank line followed by CTRL+SPACE. */
    //fillSc
    
loop:
    changeBorderColor()
    nop
    nop
    nop
    nop
    nop
    jmp loop
