#importonce
#import "common\lib\mem-global.asm"

.macro fillDefaultScreen(character) {
    /* macro from mem-global */
    //fillScreen($0400, 'x')


    /* In this file
    auto-completing the "fillScreen" macro from mem-global works sometimes.
    It works most reliably, when saving the file in a compilable state and then
    starting to type CTRL+SPACE on a blank line followed by fillSc.
    It sometimes works, when you start typing fillSc on a blank line followed by CTRL+SPACE. */

}


.macro changeBorderColor() {
    inc $d020
}